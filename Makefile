# -*- coding: utf-8 -*-
# :Project:   metapensiero.tool.banner -- Mimic SysV banner tool
# :Created:   Thu 25 Jan 2018 23:15:19 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2018 Lele Gaifax
#

all: help

include Makefile.release
