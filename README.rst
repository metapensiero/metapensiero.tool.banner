.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.tool.banner -- Mimic SysV banner tool
.. :Created:   Thu 25 Jan 2018 23:15:19 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2018 Lele Gaifax
..

==========================
 metapensiero.tool.banner
==========================

----------------------
Mimic SysV banner tool
----------------------

 :author: Lele Gaifax
 :contact: lele@metapensiero.it
 :license: GNU General Public License version 3 or later

This packages the `ActiveState banner recipe`__ into a standalone command line tool.

__ http://code.activestate.com/recipes/577537-banner/
