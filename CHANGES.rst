.. -*- coding: utf-8 -*-

Changes
-------

0.1 (2018-05-19)
~~~~~~~~~~~~~~~~

- First release


0.0 (unreleased)
~~~~~~~~~~~~~~~~

- Initial effort
